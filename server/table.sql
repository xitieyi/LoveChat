create table user
(
user_id int(10) not null auto_increment,
user_name varchar(50),
user_password varchar(50),
user_headshot varchar(300),
PRIMARY KEY (user_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table relationship
(
relationship_id int(10) not null auto_increment,
user_id_1 int(10) not null,
user_id_2 int(10) not null,
relationship_status INT(2),
PRIMARY KEY (relationship_id),
FOREIGN KEY(user_id_1) REFERENCES user(user_id),
FOREIGN KEY(user_id_2) REFERENCES user(user_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE Table relationshipInfo
(
relationshipInfo_id INT(10) not null auto_increment,
relationshipInfo_msg VARCHAR(100),
relationshipInfo_SendDate DATE,
relationshipInfo_DealDate DATE,
relationship_id INT(10) REFERENCES relationship(relationship_id)
PRIMARY KEY (relationshipInfo_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

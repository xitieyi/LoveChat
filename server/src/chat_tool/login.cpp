#include "../../include/chat_tool.h"
void chatroom_tool::login(int fd, string data)
{
    string user_name, user_password;
    Json::Reader read;
    Json::Value te;
    if (!read.parse(data, te))
    {
        cout << "json格式错误3" << endl;
        return;
    }
    if (!te.isMember("user_name"))
    {
        cout << "json状态错误4" << endl;
        return;
    }
    else
    {
        user_name = te["user_name"].asString();
    }
    if (!te.isMember("user_password"))
    {
        cout << "json状态错误5" << endl;
        return;
    }
    else
    {
        user_password = te["user_password"].asString();
    }
    Json::Value root;
    if (Mysql.log_query(user_name, user_password) == 1)
    {
        root["status"] = "login_ok";
        gun_like.log_finished(fd, user_name);
    }
    else
    {
        root["status"] = "login_fail";
    }
    string return_message = root.toStyledString();
    send(fd, return_message.c_str(), return_message.size(), 0);
}
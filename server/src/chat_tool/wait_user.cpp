#include "../../include/chat_tool.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/epoll.h>
void chatroom_tool::wait_user()
{
    sockaddr_in caddr;
    socklen_t len = 0;
    int num = epoll_wait(epfd, evs, size, -1);
    for (int i = 0; i < num; i++)
    {
        int lfd = evs[i].data.fd;
        string port;
        if (lfd == fd)
        {
            cout << fd << endl;
            // pool.acc_thread(lfd,caddr, len,epfd);
            int cfd = accept(lfd, (sockaddr *)&caddr, &len); // 加线程
            cout << "!!!" << endl;
            string ip = inet_ntoa(caddr.sin_addr);
            struct epoll_event ev;
            ev.events = EPOLLIN;
            ev.data.fd = cfd;
            cout << "客户端接入" << endl;
            int temp = epoll_ctl(epfd, EPOLL_CTL_ADD, cfd, &ev);
            gun_like.join_in(cfd);
            cout << lfd << endl;
            cout << "cfd@@@" << cfd << endl;
        }
        else
        {
            char buf[8192] = "";
            int len = recv(lfd, buf, sizeof(buf), 0);
            if (len == 0)
            {
                cout << "客户端断开连接" << endl;
                epoll_ctl(epfd, EPOLL_CTL_DEL, lfd, NULL);
                gun_like.del_disconn(lfd);
                string ip = inet_ntoa(caddr.sin_addr);
            }
            if (len < 0)
            {
                epoll_ctl(epfd, EPOLL_CTL_DEL, lfd, NULL);
                gun_like.del_disconn(lfd);
                cout << "len=" << len << endl;
                cout << "客户端异常" << endl;
            }
            if (len > 0)
            {
                string data;
                data = buf;
                cout<<strlen(buf)<<endl;
                // pool.message_deal_data.push_back(data);
                // pool.message_deal_list_fd.push_back(lfd);
                message_deal(data, lfd); // 加线程
                data.clear();
                strcpy(buf, "");
            }
        }
    }
}
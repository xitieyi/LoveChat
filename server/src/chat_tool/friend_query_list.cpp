#include "../../include/chat_tool.h"
void chatroom_tool::friend_query_list(int fd,string data)
{
    Json::Value root;
    string user_name = gun_like.heart_living(fd);
    vector<string> user_friend = Mysql.friend_query_list(user_name);
    vector<int> user_status = gun_like.friend_list_renew_able(user_friend, user_name);
    root["status"] = "friend_apply_list";
    for (int i = 0; i < user_friend.size(); i++)
    {
        Json::Value user;
        user["user_name"] = user_friend[i];
        root["friend_status"].append(user);
    }
    string return_sentence = root.toStyledString();
    send(fd, return_sentence.c_str(), return_sentence.size(), 0);
}
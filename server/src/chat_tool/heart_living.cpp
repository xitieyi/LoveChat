#include "../../include/chat_tool.h"
void chatroom_tool::heart_living(int fd, string data)
{
    Json::Value root;
    string user_name = gun_like.heart_living(fd);
    vector<string> user_friend = Mysql.friend_list_query(user_name);
    vector<int> user_status = gun_like.friend_list_renew_able(user_friend, user_name);
    root["status"] = "friend_list";
    for (int i = 0; i < user_friend.size(); i++)
    {
        Json::Value user;
        user["user_name"] = user_friend[i];
        if (user_status[i] == 1)
        {
            user["user_status"] = "live";
        }
        else
        {
            user["user_status"] = "left";
        }
        root["friend_status"].append(user);
    }
    string return_sentence = root.toStyledString();
    send(fd, return_sentence.c_str(), return_sentence.size(), 0);
}
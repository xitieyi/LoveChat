#include"../../include/chat_tool.h"
#include"../../include/jsoncpp/json/json.h"
#include<map>
#include<algorithm>
#include<string>
void chatroom_tool::message_deal(string data,int fd)
{
    string status;
    Json::Reader read;
    Json::Value root;
    if(!read.parse(data,root))
    {
        cout<<data<<endl;
        cout<<"json格式错误1"<<endl;
        return ;
    }
    if(!root.isMember("status"))
    {
        cout<<"json状态错误2"<<endl;
        return ;
    }
    else
    {
        status=root["status"].asString();
        cout<<status<<endl;
    }
    (this->*deal_map[status])(fd,data);
}
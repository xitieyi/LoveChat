#include "../../include/chat_tool.h"
void chatroom_tool::enroll(int fd, string data)
{
    string user_name, user_password;
    Json::Reader read;
    Json::Value te;
    if (!read.parse(data, te))
    {
        cout << "json格式错误" << endl;
        return;
    }
    if (!te.isMember("user_name"))
    {
        cout << "json状态错误" << endl;
        return;
    }
    else
    {
        user_name = te["user_name"].asString();
    }
    if (!te.isMember("user_password"))
    {
        cout << "json状态错误" << endl;
        return;
    }
    else
    {
        user_password = te["user_password"].asString();
    }
    Json::Value root;
    if (Mysql.enroll_query(user_name, user_password) == 1)
    {
        root["status"] = "signup_ok";
    }
    else
    {
        root["status"] = "signup_fail";
    }
    char *temp;
    string return_sentence = root.toStyledString();
    temp = new char[return_sentence.size() + 1];
    for (int i = 0; i < return_sentence.size(); i++)
    {
        temp[i] = return_sentence[i];
    }
    temp[return_sentence.size()] = '\0';
    send(fd, temp, return_sentence.size(), 0);
}
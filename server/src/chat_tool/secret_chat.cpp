#include "../../include/chat_tool.h"
void chatroom_tool::secret_chat(int fd, string data)
{
    string user_name, message, sender_name;
    sender_name = gun_like.user_search(fd);
    Json::Reader read;
    Json::Value te, root;
    if (!read.parse(data, te))
    {
        cout << "json格式错误" << endl;
        return;
    }
    if (!te.isMember("send_user"))
    {
        cout << "json状态错误" << endl;
        return;
    }
    else
    {
        user_name = te["send_user"].asString();
    }
    if (!te.isMember("send_message"))
    {
        cout << "json状态错误" << endl;
        return;
    }
    else
    {
        message = te["send_message"].asString();
    }
    int to_fd = gun_like.user_search(user_name);
    if (to_fd == -1)
    {
        root["status"] = "secret_chat_error";
        cout << "用户未登录" << endl;
        root["msg"] = "用户未登录";
    }
    else
    {
        root["status"] = "secret_chat";
        root["send_user"] = sender_name;
        root["send_message"] = message;
    }
    string temp = root.toStyledString();
    cout << "返回值：" << temp << endl;
    send(to_fd, temp.c_str(), temp.size(), 0);
}
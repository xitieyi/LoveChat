#include "../../include/mysql_tool.h"
void mysql_tool::init(const char user[], const char passwd[], const char db[])
{
    mysql_init(&mysql);
    mysql_set_character_set(&mysql, "utf8");
    if (!mysql_real_connect(&mysql, "localhost", user, passwd, db, 3306, nullptr, 0))
    {
        cout << "数据库连接失败" << endl;
        return;
    }
    else
    {
        cout << "数据库连接成功" << endl;
    }
}
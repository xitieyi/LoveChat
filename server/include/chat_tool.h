#pragma once
#include <string.h>
#include "mysql/mysql.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <string>
#include <iostream>
#include <algorithm>
#include "./user_tool.h"
#include "./mysql_tool.h"
#include "./threadpool.h"
using namespace std;
extern vector<int> fds;
class chatroom_tool
{
public:
    void init(int open_port, const char user[], const char passwd[], const char db[]); // 初始化数据库，socket,参数为“开放端口”，“用户”，“密码”，“使用表”
    void message_deal(string data, int fd);                                            // 信息处理
    void wait_user();                                                                  // 等待用户接入
    void send_all(int fd, string data);                                                // 给所有用户发送消息
    void login(int fd, string data);                                                   // 登录
    void enroll(int fd, string data);                                                  // 注册
    void heart_living(int fd, string data);                                            // 心跳机制
    void secret_chat(int fd, string data);                                             // 私聊
    void pack_send(int fd, string data);                                               // 文件传输
    void friend_query_list(int fd, string data);                                       // 好友申请列表（未同意）
    void accept_friend_query(int fd, string data);                                     // 同意好友申请
    void friend_apply_query(int fd, string data);                                      // 好友申请
private:
    MYSQL mysql;
    ThreadPool pool;
    user_tool gun_like;
    mysql_tool Mysql;
    int fd;
    int epfd;
    int size;
    struct epoll_event evs[1024];
    typedef void (chatroom_tool::*func)(int fd, string data);
    map<string, func> deal_map = {
        {"send", &chatroom_tool::send_all},
        {"signup", &chatroom_tool::enroll},
        {"login", &chatroom_tool::login},
        {"friend_list_query", &chatroom_tool::heart_living},
        {"secret_chat", &chatroom_tool::secret_chat},
        {"upload", &chatroom_tool::pack_send},
        {"friend_apply_list_quert", &chatroom_tool::friend_query_list},
        {"new_friend_apply", &chatroom_tool::friend_apply_query},
        {"new_friend_join", &chatroom_tool::accept_friend_query}};
};
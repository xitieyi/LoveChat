#include <iostream>
#include "mysql/mysql.h"
#include <vector>
#include <string.h>
using namespace std;
class mysql_tool
{
public:
    void init(const char user[], const char passwd[], const char db[]);
    int log_query(string user_name, string user_pwd);
    int enroll_query(string user_name, string user_password);
    vector<string> friend_list_query(string user_name);
    vector<string> friend_query_list(string user_name);
    int no_result(string data);
    string user_search(string user_name);

private:
    MYSQL mysql;
};
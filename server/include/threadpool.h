#pragma once
#include <iostream>
#include <thread>
#include <string.h>
#include <list>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/epoll.h>
using namespace std;

class ThreadPool
{
public:
    void judjement(void *) {}
    void to_clint(string data, int fd) {}
    void acc_thread(int fd, sockaddr_in &caddr, socklen_t &len, int epfd);
    list<int> message_deal_list_fd;
    list<string>message_deal_data;
private:
    list<void *> send_thread_list;
    list<int> send_thread_list_fd;
    list<string>send_thread_data;
    list<void *> accept_thread_list;
    list<int> accept_thread_list_fd;
    list<void *> message_deal_list;
};
#include <iostream>
#include <vector>
#include "jsoncpp/json/json.h"
#include <arpa/inet.h>
#include <sys/types.h>
#include "mysql/mysql.h"
#include <sys/epoll.h>
#include <algorithm>
using namespace std;
class user_tool
{
public:
    void join_in(int fd);                        // 默认状态码为0，字符串为空
    void log_finished(int fd, string user_name); // 加入未登录：0，加入已登录：1，未加入未登录：-1
    void del_disconn(int fd);                    // 删除当前登录用户信息
    string user_search(int fd);                  // 查找用户，返回套接字值，若返回空字符串则该用户未登录
    int user_search(string user_name);           // 查找，返回-1则为用户不存在
    vector<int> back_all_fd();                   // 返回所有套接字（已登录）（为了群聊功能暂时添加）
    string heart_living(int fd);
    void init(MYSQL Mysql);
    vector<int> friend_list_renew_able(vector<string> user_friend, string user_name); // 刷新用户列表
private:
    MYSQL mysql;
    vector<int> user;                   // fd
    vector<int> status;                 // 登录状态
    vector<string> names;               // 登录后姓名
    vector<vector<string>> user_friend; // 好友列表
    vector<vector<int>> friend_status;  // 好友状态,1为在线,0不在线
};
QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

RC_FILE = app.rc

SOURCES += \
    components/chatbox.cpp \
    components/chatbubble.cpp \
    components/clientsocket.cpp \
    components/useritem.cpp \
    main.cpp \
    pages/chat/chatwidget.cpp \
    pages/login/loginwidget.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    rec.qrc

FORMS += \
    components/useritem.ui \
    pages/chat/chatwidget.ui \
    pages/login/loginwidget.ui

HEADERS += \
    components/chatbox.h \
    components/chatbubble.h \
    components/clientsocket.h \
    components/useritem.h \
    pages/chat/chatwidget.h \
    pages/login/loginwidget.h

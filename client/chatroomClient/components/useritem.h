#ifndef USERITEM_H
#define USERITEM_H

#include <QFrame>

namespace Ui {
class UserItem;
}

class UserItem : public QFrame
{
    Q_OBJECT

public:

    explicit UserItem(QWidget *parent = nullptr);

    ~UserItem();

    void setUserName(const QString &);

    void setIsPerson(bool);

    void mouseReleaseEvent(QMouseEvent *);

signals:

    void changeUserName();

public slots:

    void setOnLine();

    void setOutLine();

    void setOn();

    void setOff();

private:

    QString user_name;

    bool status;

    bool is_person;

    Ui::UserItem *ui;

};

#endif // USERITEM_H

#ifndef CHATBUBBLE1_H
#define CHATBUBBLE1_H

#include <QLabel>
#include <QFontMetrics>

class ChatBubble : public QLabel
{
    Q_OBJECT

public:

    explicit ChatBubble(QWidget *parent = nullptr);

    ~ChatBubble();

    void setChatText(const QString &,const int);

};

#endif // CHATBUBBLE1_H

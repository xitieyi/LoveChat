#include "chatbubble.h"

ChatBubble::ChatBubble(QWidget *parent) :
    QLabel(parent)
{
    setStyleSheet("background-color: #92bd6c;\
                   border-radius: 5px;\
                   padding: 5px;");
}

ChatBubble::~ChatBubble()
{
}

void ChatBubble::setChatText(const QString &text,const int width)
{
    QFontMetrics f(font());
    QString new_text;
    int line_width = 10,wrap_count = 1,start_pos = 0,len = 1;
    for(int i = 0 ; i < text.size() ; ++i) {
        line_width = f.size(Qt::TextWordWrap,text.mid(start_pos,len)).width()+10;
        if(line_width > width) {
            new_text.push_back('\n');
            ++wrap_count;
            len = 1;
            start_pos = i;
            new_text.push_back(text.at(i));
        }
        else {
            ++len;
            new_text.push_back(text.at(i));
        }
    }
    setText(new_text);
    int text_width = wrap_count > 1 ? width : f.boundingRect(new_text).width() + 20;
    int text_height = (f.height() * wrap_count) + 10;
    setFixedSize(text_width,text_height);
}

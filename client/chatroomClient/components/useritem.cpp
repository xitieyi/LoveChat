#include "useritem.h"
#include "ui_useritem.h"
#include <QMouseEvent>
#include <QLabel>

UserItem::UserItem(QWidget *parent)
    : QFrame(parent)
    , ui(new Ui::UserItem)
{
    ui->setupUi(this);
}

UserItem::~UserItem()
{
    delete ui;
}

void UserItem::setUserName(const QString &user_name)
{
    ui->label->setText(user_name);
    this->user_name = user_name;
}

void UserItem::setOnLine()
{
    ui->label_2->setText("在线");
    ui->label_2->setStyleSheet("color: #82b33a;");
    status = true;
}

void UserItem::setOutLine()
{
    ui->label_2->setText("离线");
    ui->label_2->setStyleSheet("color: #bd3427;");
    status = false;
}

void UserItem::setIsPerson(bool is_person)
{
    // this->is_person = is_person;
}

void UserItem::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button()==Qt::LeftButton)
    {
        emit changeUserName();
    }
}

void UserItem::setOn()
{

}

void UserItem::setOff()
{

}


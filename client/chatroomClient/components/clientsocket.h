#ifndef CLIENTSOCKET_H
#define CLIENTSOCKET_H

#include <QJsonDocument>
#include <QJsonObject>
#include <QTcpSocket>
#include <QJsonArray>
#include <QFile>

class ClientSocket : public QTcpSocket
{
    Q_OBJECT
public:

    static ClientSocket *glob();

    bool isConnected() const {return is_connected;}

    void login(const QString &,const QString &);
    
    void signup(const QString &,const QString &);

    void secretChat(const QString &,const QString &);

    void friendListQuery();

    void friendApplyListQuery();

    void sendFile(const QString &,QFile &);

    bool readConfFile(const QString &);

    void connectToServer();

    ClientSocket(ClientSocket*) = delete;

    ClientSocket(ClientSocket&&) = delete;

    ClientSocket *operator =(ClientSocket*) = delete;

signals:

    void loginReply(const QJsonObject &);

    void signupReply(const QJsonObject &);

    void friendListReply(const QJsonObject &);

    void recvSecretChat(const QJsonObject &);

    void fileRead(const QJsonObject &);

    void friendApplyList(const QJsonObject &);

private slots:

    void recv();

private:

    explicit ClientSocket(QObject *parent = nullptr);

    using QTcpSocket::readyRead;

    QString server_ip = "127.0.0.1";

    unsigned short server_port = 8080;

    bool is_connected = false;

};

#endif // CLIENTSOCKET_H

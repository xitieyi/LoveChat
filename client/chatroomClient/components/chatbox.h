#ifndef CHATBOX_H
#define CHATBOX_H

#include <QStackedWidget>

class ChatBox : public QStackedWidget
{
    Q_OBJECT
public:

    explicit ChatBox(QWidget *parent = nullptr);

    void addFriend(const QString &);

    void changeChatPage(const QString &);

    void appendMessage(const QString &,Qt::AlignmentFlag);

    void appendMessageTo(const QString &,const QString &,Qt::AlignmentFlag);

signals:

private:

    QMap<QString,QWidget*> pages;

    QString current_user_name;

};

#endif // CHATBOX_H

#ifndef CHATWIDGET_H
#define CHATWIDGET_H

#include <QWidget>
#include <QTextEdit>
#include <QFile>
#include <QFileDialog>
#include "ui_chatwidget.h"
#include "components/clientsocket.h"

namespace Ui {
class ChatWidget;
}

class ChatWidget : public QWidget
{
    Q_OBJECT

public:

    explicit ChatWidget(QWidget *parent = nullptr);

    ~ChatWidget();

    void setCurrentUserName(const QString &);

    void fileRead(const QJsonObject &);

private slots:

    void on_chat_button_clicked() {ui->stackedWidget->setCurrentIndex(0);}

    void on_friend_button_clicked() {ui->stackedWidget->setCurrentIndex(1);}

    void setFriendList(const QJsonObject &);

    void receive(const QJsonObject &);

    void on_pushButton_3_clicked();

    void on_send_btn_clicked();

private:

    void connectAllSlots();

    void showEvent(QShowEvent *);

    QString send_user_name,current_user_name;

    QTimer *heart_beat_timer = nullptr;

    ClientSocket *client = ClientSocket::glob();

    Ui::ChatWidget *ui;

    struct RecvFile {
        QString file_name = "";
        QByteArray data;
        int total_size = 0;
        void clear(){
            file_name = "";
            data.clear();
            total_size = 0;
        }
    }recv_file;

};

#endif // CHATWIDGET_H

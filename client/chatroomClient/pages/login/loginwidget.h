#ifndef LOGINWIDGET_H
#define LOGINWIDGET_H

#include <QWidget>
#include "components/clientsocket.h"
#include "pages/chat/chatwidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class LoginWidget; }
QT_END_NAMESPACE

class LoginWidget : public QWidget
{
    Q_OBJECT

public:

    LoginWidget(QWidget *parent = nullptr);

    ~LoginWidget();

private slots:

    void loginReply(const QJsonObject &);

    void signupReply(const QJsonObject &);

    void on_pushbutton_login_clicked();

    void on_pushbutton_logon_clicked();

private:

    void paintEvent(QPaintEvent *);

    ChatWidget *chat_widget = nullptr;

    ClientSocket *client = ClientSocket::glob();

    Ui::LoginWidget *ui;

};
#endif // LOGINWIDGET_H

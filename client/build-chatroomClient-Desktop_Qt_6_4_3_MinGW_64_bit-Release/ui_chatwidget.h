/********************************************************************************
** Form generated from reading UI file 'chatwidget.ui'
**
** Created by: Qt User Interface Compiler version 6.4.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHATWIDGET_H
#define UI_CHATWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <components/chatbox.h>

QT_BEGIN_NAMESPACE

class Ui_ChatWidget
{
public:
    QHBoxLayout *horizontalLayout;
    QFrame *frame;
    QVBoxLayout *verticalLayout_7;
    QLabel *label;
    QLabel *label_2;
    QToolButton *chat_button;
    QToolButton *friend_button;
    QPushButton *pushButton_3;
    QSpacerItem *verticalSpacer_4;
    QToolButton *setting_button;
    QStackedWidget *stackedWidget;
    QWidget *chat_page;
    QHBoxLayout *horizontalLayout_2;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_3;
    QScrollArea *scrollArea_2;
    QWidget *scrollAreaWidgetContents_2;
    QVBoxLayout *verticalLayout_6;
    QSpacerItem *verticalSpacer_3;
    QFrame *frame_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *user_name;
    QFrame *line;
    ChatBox *widget;
    QFrame *line_2;
    QFrame *frame_5;
    QVBoxLayout *verticalLayout;
    QTextEdit *textEdit;
    QPushButton *send_btn;
    QWidget *friend_page;

    void setupUi(QWidget *ChatWidget)
    {
        if (ChatWidget->objectName().isEmpty())
            ChatWidget->setObjectName("ChatWidget");
        ChatWidget->resize(1024, 720);
        ChatWidget->setMinimumSize(QSize(579, 429));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/image/app_icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        ChatWidget->setWindowIcon(icon);
        horizontalLayout = new QHBoxLayout(ChatWidget);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName("horizontalLayout");
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        frame = new QFrame(ChatWidget);
        frame->setObjectName("frame");
        frame->setMinimumSize(QSize(60, 0));
        frame->setMaximumSize(QSize(60, 16777215));
        frame->setStyleSheet(QString::fromUtf8("background-color: rgb(46, 46, 46);"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_7 = new QVBoxLayout(frame);
        verticalLayout_7->setObjectName("verticalLayout_7");
        verticalLayout_7->setContentsMargins(0, 5, 0, 5);
        label = new QLabel(frame);
        label->setObjectName("label");
        label->setMinimumSize(QSize(50, 50));
        label->setMaximumSize(QSize(50, 50));
        label->setPixmap(QPixmap(QString::fromUtf8(":/image/test-user_headshot.png")));
        label->setScaledContents(true);

        verticalLayout_7->addWidget(label, 0, Qt::AlignHCenter);

        label_2 = new QLabel(frame);
        label_2->setObjectName("label_2");
        QFont font;
        font.setFamilies({QString::fromUtf8("Microsoft YaHei UI")});
        font.setPointSize(9);
        font.setBold(true);
        font.setItalic(false);
        label_2->setFont(font);
        label_2->setStyleSheet(QString::fromUtf8("font: 700 9pt \"Microsoft YaHei UI\";\n"
"color: rgb(255, 255, 255);"));

        verticalLayout_7->addWidget(label_2, 0, Qt::AlignHCenter);

        chat_button = new QToolButton(frame);
        chat_button->setObjectName("chat_button");
        chat_button->setMinimumSize(QSize(35, 35));
        chat_button->setMaximumSize(QSize(35, 35));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/image/test-chat.png"), QSize(), QIcon::Normal, QIcon::Off);
        chat_button->setIcon(icon1);
        chat_button->setIconSize(QSize(35, 35));
        chat_button->setPopupMode(QToolButton::DelayedPopup);
        chat_button->setAutoRaise(false);

        verticalLayout_7->addWidget(chat_button, 0, Qt::AlignHCenter);

        friend_button = new QToolButton(frame);
        friend_button->setObjectName("friend_button");
        friend_button->setMinimumSize(QSize(35, 35));
        friend_button->setMaximumSize(QSize(35, 35));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/image/test-contacts.png"), QSize(), QIcon::Normal, QIcon::Off);
        friend_button->setIcon(icon2);
        friend_button->setIconSize(QSize(35, 35));
        friend_button->setPopupMode(QToolButton::DelayedPopup);
        friend_button->setAutoRaise(false);

        verticalLayout_7->addWidget(friend_button, 0, Qt::AlignHCenter);

        pushButton_3 = new QPushButton(frame);
        pushButton_3->setObjectName("pushButton_3");
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(pushButton_3->sizePolicy().hasHeightForWidth());
        pushButton_3->setSizePolicy(sizePolicy);
        pushButton_3->setMinimumSize(QSize(35, 35));
        pushButton_3->setMaximumSize(QSize(35, 16777215));
        pushButton_3->setStyleSheet(QString::fromUtf8("border-image: url(:/image/file.png);"));

        verticalLayout_7->addWidget(pushButton_3, 0, Qt::AlignHCenter);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_4);

        setting_button = new QToolButton(frame);
        setting_button->setObjectName("setting_button");
        setting_button->setMinimumSize(QSize(35, 35));
        setting_button->setMaximumSize(QSize(35, 35));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/image/test-setting.png"), QSize(), QIcon::Normal, QIcon::Off);
        setting_button->setIcon(icon3);
        setting_button->setIconSize(QSize(35, 35));
        setting_button->setPopupMode(QToolButton::DelayedPopup);
        setting_button->setAutoRaise(false);

        verticalLayout_7->addWidget(setting_button, 0, Qt::AlignHCenter);


        horizontalLayout->addWidget(frame);

        stackedWidget = new QStackedWidget(ChatWidget);
        stackedWidget->setObjectName("stackedWidget");
        chat_page = new QWidget();
        chat_page->setObjectName("chat_page");
        horizontalLayout_2 = new QHBoxLayout(chat_page);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName("horizontalLayout_2");
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        frame_2 = new QFrame(chat_page);
        frame_2->setObjectName("frame_2");
        frame_2->setMaximumSize(QSize(200, 16777215));
        frame_2->setFrameShape(QFrame::NoFrame);
        frame_2->setFrameShadow(QFrame::Plain);
        frame_2->setLineWidth(0);
        verticalLayout_3 = new QVBoxLayout(frame_2);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setObjectName("verticalLayout_3");
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        scrollArea_2 = new QScrollArea(frame_2);
        scrollArea_2->setObjectName("scrollArea_2");
        scrollArea_2->setWidgetResizable(true);
        scrollAreaWidgetContents_2 = new QWidget();
        scrollAreaWidgetContents_2->setObjectName("scrollAreaWidgetContents_2");
        scrollAreaWidgetContents_2->setGeometry(QRect(0, 0, 198, 718));
        verticalLayout_6 = new QVBoxLayout(scrollAreaWidgetContents_2);
        verticalLayout_6->setSpacing(0);
        verticalLayout_6->setObjectName("verticalLayout_6");
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        verticalSpacer_3 = new QSpacerItem(20, 281, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_3);

        scrollArea_2->setWidget(scrollAreaWidgetContents_2);

        verticalLayout_3->addWidget(scrollArea_2);


        horizontalLayout_2->addWidget(frame_2);

        frame_3 = new QFrame(chat_page);
        frame_3->setObjectName("frame_3");
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(frame_3);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName("verticalLayout_2");
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        user_name = new QLabel(frame_3);
        user_name->setObjectName("user_name");
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(user_name->sizePolicy().hasHeightForWidth());
        user_name->setSizePolicy(sizePolicy1);
        user_name->setMinimumSize(QSize(0, 50));
        user_name->setMaximumSize(QSize(16777215, 50));
        QFont font1;
        font1.setPointSize(14);
        user_name->setFont(font1);

        verticalLayout_2->addWidget(user_name, 0, Qt::AlignHCenter);

        line = new QFrame(frame_3);
        line->setObjectName("line");
        line->setStyleSheet(QString::fromUtf8("color: rgb(227, 227, 227);"));
        line->setFrameShadow(QFrame::Plain);
        line->setFrameShape(QFrame::HLine);

        verticalLayout_2->addWidget(line);

        widget = new ChatBox(frame_3);
        widget->setObjectName("widget");
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy2);

        verticalLayout_2->addWidget(widget);

        line_2 = new QFrame(frame_3);
        line_2->setObjectName("line_2");
        line_2->setStyleSheet(QString::fromUtf8("color: rgb(227, 227, 227);"));
        line_2->setFrameShadow(QFrame::Plain);
        line_2->setFrameShape(QFrame::HLine);

        verticalLayout_2->addWidget(line_2);

        frame_5 = new QFrame(frame_3);
        frame_5->setObjectName("frame_5");
        frame_5->setMinimumSize(QSize(0, 120));
        frame_5->setMaximumSize(QSize(16777215, 120));
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame_5);
        verticalLayout->setSpacing(2);
        verticalLayout->setObjectName("verticalLayout");
        verticalLayout->setContentsMargins(0, 0, 5, 5);
        textEdit = new QTextEdit(frame_5);
        textEdit->setObjectName("textEdit");
        QFont font2;
        font2.setPointSize(12);
        textEdit->setFont(font2);
        textEdit->setStyleSheet(QString::fromUtf8("background-color: transparent;\n"
"border: none;"));

        verticalLayout->addWidget(textEdit);

        send_btn = new QPushButton(frame_5);
        send_btn->setObjectName("send_btn");
        QSizePolicy sizePolicy3(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(send_btn->sizePolicy().hasHeightForWidth());
        send_btn->setSizePolicy(sizePolicy3);
        send_btn->setMinimumSize(QSize(100, 25));
        send_btn->setMaximumSize(QSize(100, 25));
        send_btn->setFont(font2);
        send_btn->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	border-radius: 8px;\n"
"	background-color: #e9e9e9;\n"
"	color: #06ae56;\n"
"}\n"
"QPushButton:hover{\n"
"	background-color: #d2d2d2;\n"
"}\n"
"QPushButton:pressed{\n"
"	background-color: #c6c6c6;\n"
"}"));
        send_btn->setFlat(true);

        verticalLayout->addWidget(send_btn, 0, Qt::AlignRight);


        verticalLayout_2->addWidget(frame_5);


        horizontalLayout_2->addWidget(frame_3);

        stackedWidget->addWidget(chat_page);
        friend_page = new QWidget();
        friend_page->setObjectName("friend_page");
        stackedWidget->addWidget(friend_page);

        horizontalLayout->addWidget(stackedWidget);


        retranslateUi(ChatWidget);

        QMetaObject::connectSlotsByName(ChatWidget);
    } // setupUi

    void retranslateUi(QWidget *ChatWidget)
    {
        ChatWidget->setWindowTitle(QCoreApplication::translate("ChatWidget", "\347\210\261\350\201\212", nullptr));
        label->setText(QString());
        label_2->setText(QCoreApplication::translate("ChatWidget", "TextLabel", nullptr));
        chat_button->setText(QCoreApplication::translate("ChatWidget", "...", nullptr));
        friend_button->setText(QCoreApplication::translate("ChatWidget", "...", nullptr));
        pushButton_3->setText(QString());
        setting_button->setText(QCoreApplication::translate("ChatWidget", "...", nullptr));
        user_name->setText(QString());
        send_btn->setText(QCoreApplication::translate("ChatWidget", "Ctrl+S", nullptr));
#if QT_CONFIG(shortcut)
        send_btn->setShortcut(QCoreApplication::translate("ChatWidget", "Ctrl+S", nullptr));
#endif // QT_CONFIG(shortcut)
    } // retranslateUi

};

namespace Ui {
    class ChatWidget: public Ui_ChatWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHATWIDGET_H

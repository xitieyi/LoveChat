/********************************************************************************
** Form generated from reading UI file 'loginwidget.ui'
**
** Created by: Qt User Interface Compiler version 6.4.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINWIDGET_H
#define UI_LOGINWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoginWidget
{
public:
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    QWidget *widget_6;
    QWidget *widget;
    QVBoxLayout *verticalLayout_4;
    QSpacerItem *verticalSpacer_3;
    QLabel *label;
    QLineEdit *lineEdit_account;
    QLineEdit *lineEdit_password;
    QWidget *widget_4;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pushbutton_login;
    QPushButton *pushbutton_logon;
    QSpacerItem *verticalSpacer_4;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QWidget *LoginWidget)
    {
        if (LoginWidget->objectName().isEmpty())
            LoginWidget->setObjectName("LoginWidget");
        LoginWidget->resize(800, 600);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/image/app_icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        LoginWidget->setWindowIcon(icon);
        verticalLayout = new QVBoxLayout(LoginWidget);
        verticalLayout->setObjectName("verticalLayout");
        verticalSpacer = new QSpacerItem(20, 60, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer);

        frame = new QFrame(LoginWidget);
        frame->setObjectName("frame");
        frame->setStyleSheet(QString::fromUtf8(""));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName("horizontalLayout");
        horizontalLayout->setContentsMargins(100, 0, 100, 0);
        widget_6 = new QWidget(frame);
        widget_6->setObjectName("widget_6");
        widget_6->setStyleSheet(QString::fromUtf8("background-color: rgba(71, 99, 255, 100);"));

        horizontalLayout->addWidget(widget_6);

        widget = new QWidget(frame);
        widget->setObjectName("widget");
        widget->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        verticalLayout_4 = new QVBoxLayout(widget);
        verticalLayout_4->setSpacing(10);
        verticalLayout_4->setObjectName("verticalLayout_4");
        verticalLayout_4->setContentsMargins(5, 0, 5, 0);
        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_3);

        label = new QLabel(widget);
        label->setObjectName("label");
        QFont font;
        font.setPointSize(12);
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label);

        lineEdit_account = new QLineEdit(widget);
        lineEdit_account->setObjectName("lineEdit_account");
        lineEdit_account->setMinimumSize(QSize(0, 25));
        lineEdit_account->setStyleSheet(QString::fromUtf8("padding-left:5px;"));

        verticalLayout_4->addWidget(lineEdit_account);

        lineEdit_password = new QLineEdit(widget);
        lineEdit_password->setObjectName("lineEdit_password");
        lineEdit_password->setMinimumSize(QSize(0, 25));
        lineEdit_password->setStyleSheet(QString::fromUtf8("padding-left:5px;"));

        verticalLayout_4->addWidget(lineEdit_password);

        widget_4 = new QWidget(widget);
        widget_4->setObjectName("widget_4");
        horizontalLayout_3 = new QHBoxLayout(widget_4);
        horizontalLayout_3->setObjectName("horizontalLayout_3");
        pushbutton_login = new QPushButton(widget_4);
        pushbutton_login->setObjectName("pushbutton_login");
        pushbutton_login->setMinimumSize(QSize(0, 25));
        pushbutton_login->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: rgb(74, 171, 255);\n"
"	border-radius:10px;\n"
"	padding:2px 4px;\n"
"	border-style: outset;\n"
"}\n"
"QPushButton:hover{\n"
"	background-color:rgb(32, 113, 213); color: white;\n"
"}\n"
"QPushButton:pressed{\n"
"	background-color:rgb(6, 101, 255);\n"
"	border-style: inset;\n"
"}"));

        horizontalLayout_3->addWidget(pushbutton_login);

        pushbutton_logon = new QPushButton(widget_4);
        pushbutton_logon->setObjectName("pushbutton_logon");
        pushbutton_logon->setMinimumSize(QSize(0, 25));
        pushbutton_logon->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: rgb(74, 171, 255);\n"
"	border-radius:10px;padding:2px 4px;\n"
"	border-style: outset;\n"
"}\n"
"QPushButton:hover{\n"
"	background-color:rgb(32, 113, 213);\n"
"	color: white;\n"
"}\n"
"QPushButton:pressed{\n"
"	background-color:rgb(6, 101, 255);\n"
"	border-style: inset;\n"
"}"));

        horizontalLayout_3->addWidget(pushbutton_logon);


        verticalLayout_4->addWidget(widget_4);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_4);

        verticalLayout_4->setStretch(0, 1);
        verticalLayout_4->setStretch(1, 1);
        verticalLayout_4->setStretch(2, 1);
        verticalLayout_4->setStretch(3, 1);
        verticalLayout_4->setStretch(4, 1);
        verticalLayout_4->setStretch(5, 1);

        horizontalLayout->addWidget(widget);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 1);

        verticalLayout->addWidget(frame);

        verticalSpacer_2 = new QSpacerItem(20, 60, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_2);


        retranslateUi(LoginWidget);

        QMetaObject::connectSlotsByName(LoginWidget);
    } // setupUi

    void retranslateUi(QWidget *LoginWidget)
    {
        LoginWidget->setWindowTitle(QCoreApplication::translate("LoginWidget", "\347\210\261\350\201\212", nullptr));
        label->setText(QCoreApplication::translate("LoginWidget", "\347\231\273\345\275\225", nullptr));
        lineEdit_account->setPlaceholderText(QCoreApplication::translate("LoginWidget", "\350\257\267\350\276\223\345\205\245\350\264\246\345\217\267", nullptr));
        lineEdit_password->setPlaceholderText(QCoreApplication::translate("LoginWidget", "\350\257\267\350\276\223\345\205\245\345\257\206\347\240\201", nullptr));
        pushbutton_login->setText(QCoreApplication::translate("LoginWidget", "\347\231\273\345\275\225", nullptr));
        pushbutton_logon->setText(QCoreApplication::translate("LoginWidget", "\346\263\250\345\206\214", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LoginWidget: public Ui_LoginWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINWIDGET_H
